from pysnmp.entity.rfc3413.oneliner import cmdgen
import logging


class mySNMP:
    host = ""
    port = 161
    community = ""
    def __init__(self, host, community, port=161, logger=None):
        #Wlaczam logowanie - domyslny poziom DEBUG
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logger or logging.getLogger(__name__)

        self.host = host
        self.community = community
        self.port = port

#     def get_mac_from_oid(self, oid):
#         #Wydobywa MAC adres z OID'a
#         cos = str(oid[-6:]) #wybierz 6 ostatnich bajtow z OID'a
#         l = cos.split('.')
#         mac = ""
#         for a in l:
#             mac += str(hex(int(a)))[2:].upper().zfill(2)    
# 
#         return mac

    def get_oid(self, oid):
        val = 0
        cmdGen = cmdgen.CommandGenerator()
        errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
            cmdgen.CommunityData(self.community),
            cmdgen.UdpTransportTarget((self.host, self.port)),
            oid
        )

        # Check for errors and print out results
        if errorIndication:
            return errorIndication
#             self.logger.debug(errorIndication)
        elif errorStatus:
            self.logger.debug(errorStatus)
        else:
            for i in varBinds:
                val = i[1]
        return val

    def get_nextoid(self, oid):
        vals = {}
        cmdGen = cmdgen.CommandGenerator()
        errorIndication, errorStatus, errorIndex, varBinds = cmdGen.nextCmd(
            cmdgen.CommunityData(self.community),
            cmdgen.UdpTransportTarget((self.host, self.port)),
            oid
        )

        # Check for errors and print out results
        if errorIndication:
            self.logger.debug(errorIndication)
        elif errorStatus:
            self.logger.debug(errorStatus)
        else:
            for i in varBinds:
                vals[i[0][0]] = i[0][1]
        return vals
        


