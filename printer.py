from fileinput import filename


class printer:
    
    file = ''
    
    def __init__(self, file):
        self.file = file
       
        
    def read_hosts(self):
        filename = open(self.file, 'r')
        host_amount = len(filename.readlines())
        if host_amount:
            filename.seek(0)
            for i in range(0, host_amount):    
                print (i+1, '. ', str(filename.readlines()[i]).rstrip())
                filename.seek(0)
        else:
            print('There is no saved host.')
        filename.close()    
        

    def select_host(self, number):
        filename = open(self.file, 'r')
        selected_host = str(filename.readlines()[number - 1]).rstrip()
        filename.close()
        return selected_host
    
    def remove_host(self, number): 
        if number == 0:
#             filename.seek(0)
            filename = open(self.file, 'w')
            filename.truncate()
            filename.close()
        else:
            filename = open(self.file, 'r')
            lines = filename.readlines()
            filename.seek(0)
            linetodelete = filename.readlines()[number - 1]
            filename.close()
            
            filename = open(self.file, 'w')
            for line in lines:
                if line != (linetodelete):
                    filename.write(line)
            filename.close()
            
            
            


        
# pr = printer('hostlist')
# pr.read_hosts()
# print (pr.select_host(2))
# pr.remove_host(0)
