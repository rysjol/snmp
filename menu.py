# Import the modules needed to run the script.
import sys, os
from mysnmp import mySNMP
from scanner import scanner
from printer import printer
 
# Main definition - constants
main_menu_actions = {}
snmp_menu_actions = {}
hosts_menu_actions = {}
 
# =======================
#     MENUS FUNCTIONS
# =======================
 
# Main menu
def main_menu():
    os.system('cls')
    print("\n")
    print ("Welcome,\n")
    print ("Please choose the menu you want to start:")
    print ("1. SNMP")
    print ("2. Hosts management")
    print ("\n0. Quit")
    choice = input(" >>  ")
    exec_main_menu(choice)
 
    return
  
# Menu SNMP
def snmp_menu():
    print("\n")
    print ("Please choose the action\n")
    print ("1. SNMP Get")
    print ("2. SNMP Walk")
    print('\n')
    print ("9. Main menu")
    print ("0. Quit")
    choice = input(" >>  ")
    exec_snmp_menu(choice)
    return
 
 
def snmpget_menu():
    print("\n")
    print ("Please choose the action for SNMP GET\n")
    print ("1. Choose host from list")
    print ("2. Enter IP, community and OID")
    print('\n')
    print ("9. Main menu")
    print ("0. Quit")
    choice = input(" >>  ")
    exec_snmpget_menu(choice)
    return

def snmpwalk_menu():
    print("\n")
    print ("Please choose the action for SNMP WALK\n")
    print ("1. Choose host from list")
    print ("2. Enter IP, community and OID")
    print('\n')
    print ("9. Main menu")
    print ("0. Quit")
    choice = input(" >>  ")
    exec_snmpwalk_menu(choice)
    return
 
def choosehost_get():
    print("\n")
    print("Please choose host from list:\n")
    pr = printer('hostlist')
    hostlist = open('hostlist', 'r')
    amount = len(hostlist.readlines())
    hostlist.close()
    for n in range(1, amount+1):
        print(n,". ", pr.select_host(n))
    ipaddress = str(pr.select_host(int(input(" >> "))))
    print ("You set the IP address as: ", ipaddress)
    print ("Enter community name:(public)")
    community = input(" >> ")
    if community == '':
        community = 'public'
    print("You set the community as: ", community)
    print ("Enter OID:")
    oid = input (" >> ")
    if oid == '':
        oid = '1.3.6.1.2.1.1.5.0'
    print("You set the OID as: ", oid)
    hosttoget = mySNMP(ipaddress, community)
    result = hosttoget.get_oid(oid)
    print(str(result))
     
    print("\n\n")
    snmpget_menu()
 
def choosehost_walk():
    print("\n")
    print("Please choose host from list:\n")
    pr = printer('hostlist')
    hostlist = open('hostlist', 'r')
    amount = len(hostlist.readlines())
    hostlist.close()
    for n in range(1, amount+1):
        print(n,". ", pr.select_host(n))
    ipaddress = str(pr.select_host(int(input(" >> "))))
    print ("You set the IP address as: ", ipaddress)
    print ("Enter community name:(public)")
    community = input(" >> ")
    if community == '':
        community = 'public'
    print("You set the community as: ", community)
    print ("Enter OID:")
    oid = input (" >> ")
    if oid == '':
        oid = '1.3.6.1.2.1.1.9'
    print("You set the OID as: ", oid)
    hosttowalk = mySNMP(ipaddress, community)
    result = hosttowalk.get_nextoid(oid)
    for key in result.keys():
        print (str(key), str(result[key]))
     
    print("\n\n")
    snmpwalk_menu()
  
  
def enterhost_get():
    print("\n")
    print ("Enter agent IP address:")
    ipaddress = input(" >> ")
    if ipaddress == '':
        ipaddress = '172.31.31.12'
    print ("You set the IP address as: ", ipaddress)
    print ("Enter community name:(public)")
    community = input(" >> ")
    if community == '':
        community = 'public'
    print("You set the community as: ", community)
    print ("Enter OID:")
    oid = input (" >> ")
    if oid == '':
        oid = '1.3.6.1.2.1.1.5.0'
    print("You set the OID as: ", oid)
    hosttoget = mySNMP(ipaddress, community)
    result = hosttoget.get_oid(oid)
    print(str(result))
    print("\n\n")
    snmpget_menu()
             
    return

def enterhost_walk():
    print("\n")
    print ("Enter agent IP address:")
    ipaddress = input(" >> ")
    if ipaddress == '':
        ipaddress = '172.31.31.12'
    print ("You set the IP address as: ", ipaddress)
    print ("Enter community name:(public)")
    community = input(" >> ")
    if community == '':
        community = 'public'
    print("You set the community as: ", community)
    print ("Enter OID:")
    oid = input (" >> ")
    if oid == '':
        oid = '1.3.6.1.2.1.1.9'
    print("You set the OID as: ", oid)
    hosttowalk = mySNMP(ipaddress, community)
    result = hosttowalk.get_nextoid(oid)
    for key in result.keys():
        print (str(key), str(result[key]))
    print("\n\n")
    snmpwalk_menu()
             
    return
 
 
# Menu HOST
def hosts_menu():
    print("\n")
    print ("Please choose the action\n")
    print ("1. Show saved hosts")
    print ("2. Scan for new hosts")
    print ("3. Forget specific hosts")
    print ("4. Forget all hosts")
    print('\n')
    print ("9. Main menu")
    print ("0. Quit") 
    choice = input(" >>  ")
    exec_hosts_menu(choice)
    return

def show_hosts():
    print("\n")
    hosts = printer('hostlist')
    hosts.read_hosts()
    hosts_menu()
    
def search_hosts():
    print('\nPlease enter NETWORK address to scan. Use syntax >NETWORK ADDRESS< / >NETMASK>.')
    network = input(' >> ')
    if network == '':
        network = '172.31.31.8/29'
    print("You set the network as: ", network)    
    
    print('Please enter community name.')
    community = input(' >> ')
    if community == '':
        community = 'public'
    print("You set the community as: \n", community)
    scan1 = scanner(network, community)
    scan1.scan_network()
    hosts_menu()
         
    
def forget_host():
    print('\n')
    print("Please select host to remove:")
    print('\n')
    hosts = printer('hostlist')
    hosts.read_hosts()
    print('\n')
    selected_host = int(input(' >> '))
    print('\n')
    try:
        hosts.remove_host(selected_host)
        print("Host", selected_host, " removed from saved host.")
    except:
        print('Incorrect value. Please try again.')
        hosts_menu()
    hosts_menu()
    
def forget_all_hosts():
    hosts = printer('hostlist')
    hosts.remove_host(0)
    print('\nAll saved hosts have been removed.')
    hosts_menu()
    
    
# Execute main menu
def exec_main_menu(choice):
    os.system('cls')
    ch = choice.lower()
    if ch == '':
        main_menu_actions['main_menu']()
    else:
        try:
            main_menu_actions[ch]()
        except KeyError:
            print ("Invalid selection, please try again.\n")
            main_menu_actions['main_menu']()
    return
 
# execute menu SNMP
def exec_snmp_menu(choice):
    os.system('cls')
    ch_snmp = choice.lower()
    if ch_snmp == '':
        snmp_menu_actions['snmp_menu']()
    else:
        try:
            snmp_menu_actions[ch_snmp]()
        except KeyError:
            print ("Invalid selection, please try again.\n")
            snmp_menu_actions['snmp_menu']()
    return

def exec_snmpget_menu(choice):
    os.system('cls')
    ch_snmpget = choice.lower()
    if ch_snmpget == '':
        snmpget_menu_actions['snmpget_menu']()
    else:
        try:
            snmpget_menu_actions[ch_snmpget]()
        except KeyError:
            print ("Invalid selection, please try again.\n")
            snmpget_menu_actions['snmpget_menu']()
    return

def exec_snmpwalk_menu(choice):
    os.system('cls')
    ch_snmpwalk = choice.lower()
    if ch_snmpwalk == '':
        snmpwalk_menu_actions['snmpwalk_menu']()
    else:
        try:
            snmpwalk_menu_actions[ch_snmpwalk]()
        except KeyError:
            print ("Invalid selection, please try again.\n")
            snmpwalk_menu_actions['snmpwalk_menu']()
    return


# execute menu hosts
def exec_hosts_menu(choice):
    os.system('cls')
    ch_hosts = choice.lower()
    if ch_hosts == '':
        hosts_menu_actions['hosts_menu']()
    else:
        try:
            hosts_menu_actions[ch_hosts]()
        except KeyError:
            print ("Invalid selection, please try again.\n")
            hosts_menu_actions['hosts_menu']()
    return
 
# Back to main menu
def back():
    main_menu_actions['main_menu']()
 
# Exit program
def exit_app():
    print("\n\n")
    print("See you again!")
    sys.exit()
 
# =======================
#    MENUS DEFINITIONS
# =======================
 
# SNMP Menu definition
main_menu_actions = {
    'main_menu': main_menu,
    '1': snmp_menu,
    '2': hosts_menu,
    '9': back,
    '0': exit_app,
}

# SNMP Menu definition
snmp_menu_actions = {
    'snmp_menu': snmp_menu,
    '1': snmpget_menu,
    '2': snmpwalk_menu,
    '9': back,
    '0': exit_app,
}

#SNMPGET Menu def
snmpget_menu_actions = {
    'snmpget_menu': snmpget_menu,
    '1': choosehost_get,
    '2': enterhost_get,
    '9': back,
    '0': exit_app,
}

#SNMPWALK Menu def
snmpwalk_menu_actions = {
    'snmpwalk_menu': snmpwalk_menu,
    '1': choosehost_walk,
    '2': enterhost_walk,
    '9': back,
    '0': exit_app,
}


# SNMP Menu definition
hosts_menu_actions = {
    'hosts_menu': hosts_menu,
    '1': show_hosts,
    '2': search_hosts,
    '3': forget_host,
    '4': forget_all_hosts,
    '9': back,
    '0': exit_app,
}
 
# =======================
#      MAIN PROGRAM
# =======================
 
# Main Program
if __name__ == "__main__":
    # Launch main menu
    main_menu()
