from mysnmp import mySNMP
from netaddr import IPNetwork
import nmap
import logging
from unittest import result

        
class scanner:
    network = ''
    port = 161
    community = ""
    def __init__(self, network, community, port=161, logger=None):
        #Wlaczam logowanie - domyslny poziom DEBUG
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logger or logging.getLogger(__name__)

        self.network = network
        self.community = community
        self.port = port

    def scan_network(self):
        hostlist = open("hostlist", 'a')
        nm = nmap.PortScanner()        
        for loop_1 in IPNetwork(self.network):
            nm.scan(loop_1.format(), '161')
            try:
                state = nm[loop_1.format()].has_tcp(161)
                if state == True:
                    s1 = mySNMP(loop_1.format(), self.community)
                    result = s1.get_oid('1.3.6.1.2.1.1.5.0')
                    result = str(result)
                    if result == "No SNMP response received before timeout":
                        print (loop_1.format() , " is reachable but has CLOSED UDP port 161.")
                    else:
                        print (loop_1.format() , " has OPENED UDP port 161. Hostname is " , result )
                        hostlist.write(loop_1.format() + '\n')
                else:
                    pass
            except KeyError:
                pass
        print ("Other hosts from network ", self.network, " are unreachable.")
        hostlist.close()

# scan1 = scanner('172.31.31.8/29', 'public')
# scan1.scan_network()
#          
        